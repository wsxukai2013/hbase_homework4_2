package hhasehomework.hbase_homework4_2;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
//import org.apache.hadoop.hbase.mapred.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
//import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

/**
 * Hello world!
 *
 
public class HbaseHomework2 {
	public static Long timeToLong(String s_t) throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat t = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
		Date time = t.parse(s_t);
		return time.getTime();
	}

	public static Long ipToLong(String s_ip) {
		// TODO Auto-generated method stub
		Long[] ip = new Long[4];
		int p1 = s_ip.indexOf(".");
		int p2 = s_ip.indexOf(".", p1 + 1);
		int p3 = s_ip.indexOf(".", p2 + 1);
		ip[0] = Long.parseLong(s_ip.substring(0, p1));
		ip[1] = Long.parseLong(s_ip, p1 + 1);
		ip[2] = Long.parseLong(s_ip, p2 + 1);
		ip[3] = Long.parseLong(s_ip, p3 + 1);
		return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + ip[3];
	}

	public static class Map extends Mapper<Object, Text, Text, Text> {
		Text outputKey = null;
		Text outputValue = null;

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String valueString = value.toString();
			String[] items = valueString.split(" ");

			outputKey = new Text(items[0] + "-" + items[3].substring(1));
			outputValue = new Text(items[6]);
			context.write(outputKey, outputValue);
			// System.out.println("Map is : "+" key1:"+outputKey.toString()+" ****Value1:"
			// +outputValue.toString());
		}
	}

	public static class Reduce extends
			Reducer<Text, Text, ImmutableBytesWritable, ImmutableBytesWritable> {
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException, ParseException {
			String[] s = key.toString().split("-");
			Long ip = ipToLong(s[0]);
			Long time = timeToLong(s[1]);

			for (Text val : values) {
				Put p = new Put(Bytes.toBytes(ip.toString() + "-"+ time.toString()));
				p.add(Bytes.toBytes("info"), Bytes.toBytes("url"),
						Bytes.toBytes(val.toString()));
			}
		}
	}

	public static Job createSubmittableJob(Configuration conf, String[] args) throws IOException {
		String tableName = args[0];
		Path inputDir = new Path(args[1]);

		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "HbaseHomework2");
		job.setJarByClass(HbaseHomework2.class);
		job.setMapperClass(Map.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		// FileInputFormat.setInputPaths(job, inputDir);
		job.setOutputKeyClass(ImmutableBytesWritable.class);
		job.setOutputValueClass(ImmutableBytesWritable.class);
		TableMapReduceUtil.initTableReducerJob(tableName, null, job);
		job.setNumReduceTasks(3);
		// TableMapReduceUtil.addDependencyJars(job);
		FileInputFormat.addInputPath(job, inputDir);
		return job;
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		if (args.length<2){
			System.err.println("Usage : HbaseHomework2 <tablename> <inputpath>");
			System.exit(2);
		}
		Job job = createSubmittableJob(conf, args);
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
*/


public class HbaseHomework2 {
	public static Long timeToLong(String s_t) throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat t = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
		Date time = t.parse(s_t);
		return time.getTime();
	}

	public static Long ipToLong(String s_ip) {
		// TODO Auto-generated method stub
		Long[] ip = new Long[4];
		int p1 = s_ip.indexOf(".");
		int p2 = s_ip.indexOf(".", p1 + 1);
		int p3 = s_ip.indexOf(".", p2 + 1);
		ip[0] = Long.parseLong(s_ip.substring(0, p1));
		ip[1] = Long.parseLong(s_ip, p1 + 1);
		ip[2] = Long.parseLong(s_ip, p2 + 1);
		ip[3] = Long.parseLong(s_ip, p3 + 1);
		return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + ip[3];
	}

	public static class Map extends Mapper<Object, Text,ImmutableBytesWritable, Put> {
		//Text outputKey = null;
		//Text outputValue = null;
		String rowkey=null;
		//Long ip = null;                   
		//Long time = null;

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String valueString = value.toString();
			String[] items = valueString.split(" ");   
			rowkey=items[0] + "-" + items[3].substring(1);
			ImmutableBytesWritable rowKey =  new ImmutableBytesWritable(Bytes.toBytes(rowkey));
			Put p = new Put(rowKey.get());
			//outputKey = new Text(items[0] + "-" + items[3].substring(1));
			//outputValue = new Text(items[6]);
			p.add(Bytes.toBytes("info"), Bytes.toBytes("url"),Bytes.toBytes(items[6].toString()));
			context.write(rowKey,p);
			// System.out.println("Map is : "+" key1:"+outputKey.toString()+" ****Value1:"
			// +outputValue.toString());
		}
	}

	public static class Reduce extends
			Reducer<Text, IntWritable, ImmutableBytesWritable, ImmutableBytesWritable> {
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException, ParseException {
			String[] s = key.toString().split("-");
			Long ip = ipToLong(s[0]);
			Long time = timeToLong(s[1]);

			for (Text val : values) {
				Put p = new Put(Bytes.toBytes(ip.toString() + "-"+ time.toString()));
				p.add(Bytes.toBytes("info"), Bytes.toBytes("url"),
						Bytes.toBytes(val.toString()));
			}
		}
	}

	public static Job createSubmittableJob(Configuration conf, String[] args) throws IOException {
		String tableName = args[0];
		Path inputDir = new Path(args[1]);

		@SuppressWarnings("deprecation")
		Job job = new Job(conf,"HbaseHomework2");
		job.setJarByClass(HbaseHomework2.class);
		job.setMapperClass(Map.class);
		//job.setMapOutputKeyClass(Text.class);
		//job.setMapOutputValueClass(Text.class);
		FileInputFormat.setInputPaths(job, inputDir);
		job.setInputFormatClass(TextInputFormat.class);
		//job.setOutputKeyClass(ImmutableBytesWritable.class);
		//job.setOutputValueClass(ImmutableBytesWritable.class);
		TableMapReduceUtil.initTableReducerJob(tableName, null, job);
		job.setNumReduceTasks(0);
		TableMapReduceUtil.addDependencyJars(job);
		// TableMapReduceUtil.addDependencyJars(job);
		//FileInputFormat.addInputPath(job, inputDir);
		return job;
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		if (args.length<2){
			System.err.println("Usage : HbaseHomework2 <tablename> <inputpath>");
			System.exit(2);
		}
		Job job = createSubmittableJob(conf, args);
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}



	






 

 


